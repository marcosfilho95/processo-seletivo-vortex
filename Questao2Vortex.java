import java.util.Scanner;
import java.util.Random;

public class Questao2Vortex {
    public static void main(String args[]){

        Scanner scanner = new Scanner(System.in);

        int quantestrelas,estrelaum,estreladois;

        Random rand = new Random();

        String entrada, listaentrada[];

        boolean possuiligacao = false;

        entrada = scanner.nextLine();

        entrada = entrada.replace("{", "");
        entrada = entrada.replace("}", "");
        entrada = entrada.replace(" ", "");
        listaentrada = entrada.split(",");

        scanner.close();

        quantestrelas = Integer.parseInt(listaentrada[0]);
        estrelaum = Integer.parseInt(listaentrada[1]);
        estreladois = Integer.parseInt(listaentrada[2]);

        int[][] matriz = new int[quantestrelas][quantestrelas];

        System.out.println("[");
        System.out.println("");
        for(int x = 0; x<quantestrelas;x++){
            for(int y = 0; y<quantestrelas;y++){
                matriz[x][y] = 0;
            }
        }
        for(int x=0;x<quantestrelas;x++){
            System.out.print("[ ");
            for(int y=0; y<quantestrelas;y++){
                if(x == y){
                    matriz[x][y] = 0;
                }
                else{
                    if(x==0){
                        matriz[x][y] = rand.nextInt(2);
                        if((matriz[x][y] == 1) && ((x ==estrelaum && y == estreladois) || (y ==estrelaum && x == estreladois))){
                            possuiligacao = true;
                        }
                    }
                    else{
                        if(x>y){
                            if(matriz[y][x] == 1){
                                matriz[x][y] = 1;
                            }
                            else{
                                matriz[x][y] = 0;
                            }
                        }
                        else{
                            matriz[x][y] = rand.nextInt(2);
                            if((matriz[x][y] == 1) && ((x ==estrelaum && y == estreladois) || (y ==estrelaum && x == estreladois))){
                                possuiligacao = true;
                            }
                        }
                    }
                }
                System.out.print(matriz[x][y]+", ");
            }
            if(x!= quantestrelas-1){
                System.out.println("],");
            }
            else{
                System.out.println("]");
            }
        }
        System.out.println("");
        if(possuiligacao){
            System.out.println("], há ligação");
        }
        else{
            System.out.println("], não há ligação");
        }
    }
}
